import React, {
  createContext,
  useState,
  useContext,
  ReactNode,
} from 'react';

const OrdersContext = createContext({} as any);

export const OrdersProvider = ({ children }: { children: ReactNode }) => {
  const [orders, setOrders] = useState(-1);

  return (
    <OrdersContext.Provider value={{ orders, setOrders }}>
      {children}
    </OrdersContext.Provider>
  );
};

export const useOrders = () => useContext(OrdersContext);
