import { useState, useEffect, FC } from 'react';
import Link from 'next/link';
import styles from '../styles/Home.module.css'
import { useOrders } from './useOrders';

const Details = ({ id, order }) => {

  return (
    <>
      <p>Order: {id}</p>
      {order && <p>{order}</p>}
    </>
  )
}

const OrdersContainer: FC<{fetchedOrders: any[], id?: string}> = ({ fetchedOrders, id, children }) => {

  const {orders, setOrders} = useOrders();
  const [activeOrder, setActiveOrder] = useState(null);

  useEffect(()=>{

    if(fetchedOrders) setOrders(fetchedOrders);
  }, [fetchedOrders]);

  useEffect(()=>{

    console.log("ORDERS CHANGED IN PROVIDER ", orders);
    // This is not required, but I'm logging this to show when it changes
  }, [orders]);

  useEffect(()=>{

    if(orders) setActiveOrder(orders[0]);
    // Here you would actually filter the orders
    // orders.find((el)=> el.id === id)
    // to get the right one, and set the active tab as well
  }, [id]);

  return (
    <div className={styles.grid}>
    
      {/* Just a bunch of links to navigate and test */}
      <Link href={{
        pathname: '/orders/[id]',
        query: { id: 1 }
      }}>
        <a className={styles.card}>
          <h2>Order 1 &rarr;</h2>
          <p>(Don't forget to read logs).</p>
        </a>
      </Link>

      <Link href={{
        pathname: '/orders/[id]',
        query: { id: 2 }
      }}>
        <a className={styles.card}>
          <h2>Order 2 &rarr;</h2>
          <p>(Don't forget to read logs).</p>
        </a>
      </Link>

      <Link href={{ pathname: '/orders' }}>
        <a className={styles.card}>
          <h2>Orders Global &rarr;</h2>
          <p>(Don't forget to read logs).</p>
        </a>
      </Link>

      {/* The actual children */}
      {activeOrder && <Details id={id} order={activeOrder} />}
    </div>
  )
}

export default OrdersContainer;