import { useEffect } from 'react';
import OrdersContainer from '../../containers/orders';

const Orders = ({ arrList, url }) => {

  useEffect(()=>{
    console.log(arrList, url);
  }, [arrList]);

  return (
    <OrdersContainer fetchedOrders={arrList}></OrdersContainer>
  )
}

export const getServerSideProps = async (ctx) => {

  const { url } = ctx.req;
  let arrList;

  if(url.endsWith(".json")){
    // This was navigation
    // So here you don't fetch anything
    arrList = ["This page was navigated"];
  }
  else{
    // This was full refresh
    // This is where you will need to fetch
    arrList = ["This page was full refreshed"];
  }

  return { props: { arrList, url } };
}

export default Orders;