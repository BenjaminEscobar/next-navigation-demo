import { useEffect } from 'react';
import { useRouter } from 'next/router'
import OrdersContainer from '../../containers/orders';

const OrderDetail = ({ arrList, url }) => {
  const router = useRouter()
  const { id } = router.query

  useEffect(()=>{
    console.log(arrList, url);
  }, [arrList]);

  return (
    <OrdersContainer id={id as string} fetchedOrders={arrList}></OrdersContainer>
  )
}

export const getServerSideProps = async (ctx) => {

  const { url } = ctx.req;
  let arrList;

  if(/.json\?id=/.test(url)){
    // This was navigation
    // So here you don't fetch anything
    arrList = ["This page was navigated"];
  }
  else{
    // This was full refresh
    // This is where you will need to fetch
    arrList = ["This page was full refreshed"];
  }

  return { props: { arrList, url } };
}

export default OrderDetail;