import Head from 'next/head'
import Link from 'next/link';
import styles from '../styles/Home.module.css'
import MainLayout from '../layouts/MainLayout';

const Home = () => {
  return (
    <MainLayout>
      <Head>
        <title>Home Page</title>
      </Head>

      <h1 className={styles.title}>
        Welcome to <a href="https://nextjs.org">Next.js!</a>
      </h1>

      <p className={styles.description}>
        Try below links and while looking at developer's console.
      </p>

      <div className={styles.grid}>

        <Link href={{
          pathname: '/orders/[id]',
          query: { id: 1 }
        }}>
          <a className={styles.card}>
            <h2>Order 1 &rarr;</h2>
            <p>(Don't forget to read logs).</p>
          </a>
        </Link>

        <Link href={{
          pathname: '/orders/[id]',
          query: { id: 2 }
        }}>
          <a className={styles.card}>
            <h2>Order 2 &rarr;</h2>
            <p>(Don't forget to read logs).</p>
          </a>
        </Link>

        <Link href={{ pathname: '/orders' }}>
          <a className={styles.card}>
            <h2>Orders Global &rarr;</h2>
            <p>(Don't forget to read logs).</p>
          </a>
        </Link>

      </div>

    </MainLayout>
  )
}

export default Home;