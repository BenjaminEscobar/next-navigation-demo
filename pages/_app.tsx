import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { OrdersProvider } from '../containers/useOrders';

function MyApp({ Component, pageProps }: AppProps) {

  return (
    <OrdersProvider>
      <Component {...pageProps} />
    </OrdersProvider>
  )
}
export default MyApp
